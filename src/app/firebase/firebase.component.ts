import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-firebase',
  templateUrl: './firebase.component.html',
  styleUrls: ['./firebase.component.scss']
})
export class FirebaseComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  routeToFirestore() {
    this.router.navigate(["/firestore"]);
  }

  routeToStorage() {
    this.router.navigate(["/storage"])
  }

  routeToAuth() {
    this.router.navigate(["/auth"])
  }

}
