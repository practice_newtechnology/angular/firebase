import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FbStorageComponent } from './fb-storage.component';

describe('FbStorageComponent', () => {
  let component: FbStorageComponent;
  let fixture: ComponentFixture<FbStorageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FbStorageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FbStorageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
