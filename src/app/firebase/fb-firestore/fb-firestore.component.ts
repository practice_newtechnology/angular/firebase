import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-fb-firestore',
  templateUrl: './fb-firestore.component.html',
  styleUrls: ['./fb-firestore.component.scss']
})
export class FbFirestoreComponent implements OnInit {
  items: Observable<any[]>;

  constructor(firestore: AngularFirestore) {
    this.items = firestore.collection('/items').valueChanges();
    this.items.subscribe(data => {
      console.log(data)
    })
  }

  ngOnInit(): void {
  }

  notes() {
    /*
    
    <h2>Items for {{ (profile | async)?.name }}
    <ul>
      <li *ngFor="let item of items | async">{{ item.name }}</li>
    </ul>
    <div class="control-input">
      <input type="text" #itemname />
      <button (click)="addItem(itemname.value)">Add Item</button>
    </div>

    private readonly itemsRef: AngularFirestoreCollection<Item>;
    private readonly profileRef: AngularFirestoreDocument<Profile>;
    items: Observable<Item[]>;
    profile: Observable<Profile>;

    ngOnInit() {
      this.itemsRef = afs.collection('items', ref => ref.where('user', '==', 'davideast').limit(10));
      this.items = this.itemsRef.valueChanges().map(snap => snap.docs.map(data => doc.data()));
      this.items = from(this.itemsRef);
      this.profileRef = afs.doc('users/davideast');
      this.profile = this.profileRef.valueChanges();
    }

    addItem(name: string) {
      const user = 'davideast';
      this.itemsRef.add({ name, user });
    }

    */
  }


}
