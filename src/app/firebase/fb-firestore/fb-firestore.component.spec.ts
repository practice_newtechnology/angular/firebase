import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FbFirestoreComponent } from './fb-firestore.component';

describe('FbFirestoreComponent', () => {
  let component: FbFirestoreComponent;
  let fixture: ComponentFixture<FbFirestoreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FbFirestoreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FbFirestoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
