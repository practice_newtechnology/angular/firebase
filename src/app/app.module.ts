import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FirebaseComponent } from './firebase/firebase.component';
import { FbAuthComponent } from './firebase/fb-auth/fb-auth.component';
import { FbFirestoreComponent } from './firebase/fb-firestore/fb-firestore.component';
import { FbStorageComponent } from './firebase/fb-storage/fb-storage.component';
import { AngularFireModule } from '@angular/fire';
import { environment } from 'src/environments/environment';
import { AngularFirestoreModule } from '@angular/fire/firestore';

@NgModule({
  declarations: [
    AppComponent,
    FirebaseComponent,
    FbAuthComponent,
    FbFirestoreComponent,
    FbStorageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFirestoreModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
