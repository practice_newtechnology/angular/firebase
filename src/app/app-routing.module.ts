import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FbAuthComponent } from './firebase/fb-auth/fb-auth.component';
import { FbFirestoreComponent } from './firebase/fb-firestore/fb-firestore.component';
import { FbStorageComponent } from './firebase/fb-storage/fb-storage.component';
import { FirebaseComponent } from './firebase/firebase.component';

const routes: Routes = [
  { path: '', component: FirebaseComponent},
  { path: 'auth', component: FbAuthComponent },
  { path: 'firestore', component: FbFirestoreComponent },
  { path: 'storage', component: FbStorageComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
