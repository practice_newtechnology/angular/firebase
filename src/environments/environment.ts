// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyBS5bg0GhLBI-XaW12rb9CbnmV-CjJbrL4",
    authDomain: "angularfirebase-8cdc5.firebaseapp.com",
    databaseURL: "https://angularfirebase-8cdc5.firebaseio.com",
    projectId: "angularfirebase-8cdc5",
    storageBucket: "angularfirebase-8cdc5.appspot.com",
    messagingSenderId: "435326507325",
    appId: "1:435326507325:web:c3a53692e703f11dc88d54",
    measurementId: "G-N116C0TZDB"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
